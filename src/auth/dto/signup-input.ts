import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class SignupUserInput {
  @Field()
  // @IsEmail()
  email: string;

  @Field()
  password: string;

  @Field()
  username: string;

  @Field({ nullable: true })
  name: string;

  @Field()
  createdAt: string = new Date().toISOString();

  @Field()
  updatedAt: string = new Date().toISOString();
}
