import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { GraphQLError } from 'graphql';
import { AuthService } from './auth.service';
import { LoginUserInput, LoginResponse, SignupUserInput } from './dto';
import { User } from '../user/entity/user.entity';

@Resolver()
export class AuthResolver {
  constructor(private authService: AuthService) {}

  @Mutation(() => LoginResponse)
  async login(
    @Args('loginUserInput') loginUserInput: LoginUserInput,
  ): Promise<GraphQLError | LoginResponse> {
    try {
      return await this.authService.login(loginUserInput);
    } catch (err) {
      console.error(err);
    }
  }

  @Mutation(() => User)
  async signup(@Args('signupUserInput') signupUserInput: SignupUserInput) {
    try {
      return await this.authService.create(signupUserInput);
    } catch (err) {
      console.error(err);
    }
  }
}
