import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { JwtService } from '@nestjs/jwt';
import { GraphQLError } from 'graphql';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import { User, UserDocument, Roles } from 'src/user/entity/user.entity';
import { SignupUserInput, LoginResponse } from './dto';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    @InjectModel(User.name) private UserModel: Model<UserDocument>,
  ) {}

  authToken = null;

  async create(signupUserInput: SignupUserInput) {
    try {
      const isUser = await this.UserModel.findOne({
        email: signupUserInput.email,
      });
      if (isUser) {
        throw new GraphQLError('Nah Bro, you already exist 🤡');
      } else {
        signupUserInput.password = await bcrypt
          .hash(signupUserInput.password, 10)
          .then((r) => r);
        const newUser = { ...signupUserInput, role: Roles.GUEST };
        return await new this.UserModel(newUser).save();
      }
    } catch (err) {
      console.error(err);
    }
  }

  async getToken(email, _id): Promise<string> {
    try {
      const token = await this.jwtService.signAsync({ email, _id });
      return token;
    } catch (err) {
      console.error(err);
    }
  }

  async login({ password, email }): Promise<GraphQLError | LoginResponse> {
    try {
      const res = await this.UserModel.findOne({ email });
      const user = res && {
        _id: res._id,
        email: res.email,
        username: res.username,
        name: res.name,
        role: res.role,
        createdAt: res.createdAt,
        updatedAt: res.updatedAt,
      };

      this.getToken(email, res._id).then((result) => {
        this.authToken = result;
      });

      const valid = res && (await bcrypt.compare(password, res.password));

      if (valid) {
        return {
          access_token: this.authToken,
          user,
        };
      } else {
        new GraphQLError('Invalid credentials. Please try again.');
      }
    } catch (err) {
      console.error(err);
    }
  }
}
