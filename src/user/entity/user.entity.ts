import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';
import { Document, Types } from 'mongoose';

export enum Roles {
  GUEST = 'guest',
  ADMIN = 'admin',
  MANAGER = 'manager',
}

registerEnumType(Roles, {
  name: 'Roles',
  description: 'Admin create projects & tasks, Manager create tasks',
});

@ObjectType()
@Schema()
export class User {
  @Field(() => String)
  _id: Types.ObjectId;

  @Field()
  @Prop()
  email: string; /** TODO: сделать уникальным */

  @Field()
  @Prop()
  password: string;

  @Field({ nullable: true })
  @Prop()
  name: string;

  @Field()
  @Prop()
  username: string;

  @Field(() => Roles, { defaultValue: Roles.GUEST, nullable: true })
  @Prop()
  role: Roles;

  @Field()
  @Prop()
  createdAt: string = new Date().toISOString();

  @Field()
  @Prop()
  updatedAt: string = new Date().toISOString();
}

@ObjectType()
export class Token {
  @Field({ nullable: true })
  token: string;
}

export type UserDocument = User & Document;

export const UserSchema = SchemaFactory.createForClass(User);
