import { InputType, OmitType, PartialType } from '@nestjs/graphql';
import { SignupUserInput } from '../../auth/dto';

@InputType()
export class UpdateUserInput extends PartialType(
  OmitType(SignupUserInput, ['password'] as const),
) {}
